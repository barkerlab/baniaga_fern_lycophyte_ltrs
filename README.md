## This repository is archival only and no longer updated. New updates will be made at https://gitlab.com/barker-lab/Baniaga_Fern_Lycophyte_LTRs 

##A repository of scripts used in the analayses described in Baniaga & Barker (in review):
 - Baniaga, A.E. and M. S. Barker. Nuclear genome size is positively correlated with mean LTR insertion date in fern and lycophyte genomes. In review.


----------


#####LTR_FullExtract.pl
This perl script will output a full length LTR from 5' to 3' end given its coordinates output from LTR_finder

Example usage:
```
perl LTR_FullExtract.pl modFastaSequence.fa coordsFull.txt
```


'modFastaSequence.fa' is a modifed fasta file of the assembled genome input to LTR_Finder
It has been modified by:

 - Removing the '>' from the beginning of each line (use sed) +4
 - Fasta id and sequence are on the same line seperated by a space (use awk 'ORS=NR%2?" ":"\n"' inputFasta > modifiedFasta)


'coordsFull.txt' is a file that describes the name of a contig and the coordinates of the LTR

This should be in the tab delimited format of:
```
contig.num start end
```

'contig.num' is a record of the name of a fasta sequence and a consecutive 'num' when more than one LTR is found in a particular fasta sequence

 - Also remember that coordinates need to have 1 added to them from the LTR_finder output because perl is 0 indexed
 - Also remember that overlapping LTRs need to be manually removed based on their overlapping coordinates


#####Output

Output all the fastas into their own files, then each LTR as a *full* file


----------


#####LTR_SidedExtract.pl
This perl script will output seperate files of the 5' and 3' ends of an LTR given coordinates

Example usage:
```
perl LTR_SidedExtract.pl modFastaSequence.fa coordsSided.txt
```

'modFastaSequence.fa' is the same as above

'coordsFull.txt' is a file that describes the name of a contig, the coordinates of the LTR, and whether it is of 5' or 3' end

This should be in the tab delimited format of:
```
contig.num start end name
```

'contig.num' is a record of the name of a fasta sequence and a consecutive 'num' when more than one LTR is found in a particular fasta sequence

'name' is either a 5 or 3 that denotes whether the sequence is of the 5' or 3' end of an LTR 

 - Also remember that coordinates need to have 1 added to them from the LTR_finder output because perl is 0 indexed
 - Also remember that overlapping LTRs need to be manually removed based on their overlapping coordinates


#####Output

Outputs all the fastas into their own files, then each LTR as a *sided* file with the '_LTR' suffix, and a fasta file with labeled 5' and 3' ends as sequences

----------


Downstream quick processing of these files may include:
 - Removing the space between letters
```
sed -i 's/ //g' *LTR.fa
```
```
sed -i '1s/$/_5/' *_LTR.fa
```
```
sed -i '3s/$/_3/' *_LTR.fa
```
